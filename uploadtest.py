from requests import get, post, patch
from os import path
from settings import *
from api_token import authenticate, get_header

authenticate()
head = get_header()

'''
files = {'upload_file': open('file.txt','rb')}
values = {'DB': 'photcat', 'OUT': 'csv', 'SHORT': 'short'}

r = requests.post(url, files=files, data=values)

curl -X POST \
-H "Content-Type: multipart/form-data" \
-H "Authorization: Bearer ${AUTH_TOKEN}" \
-F attached_file=@test.png \
-F from_comment=False \
-F object_id=1 \
-F project=1 \
-s http://localhost:8000/api/v1/userstories/attachments

'''


def upload(filename, userstory_id):
    desc = ""
    files = {"attached_file": open(filename, 'rb')}
    values = {"from_comment": "False",
              "object_id": str(userstory_id),
              "project": "6",
              "description": str(desc)}

    attachUrl = SERVER_NAME + "api/v1/userstories/attachments"
    attachmentResponse = post(attachUrl, headers=head, files=files,
                              data=values)
    # attachmentResponse = post(attachUrl, headers=head,
    #    data=values, files=files)

    print(attachUrl)
    print(attachmentResponse.text)
    print(attachmentResponse.json())
    print(attachmentResponse)


def change_status(userstory_id, version):
    values = {"version": str(version),
              "status": str(KANBAN_COLUMN_ID + 1)}
    us_url = SERVER_NAME + "api/v1/userstories/" + str(userstory_id)
    usResponse = patch(us_url, headers=head, data=values)
    print(usResponse.text)
    print(usResponse.json())


def create_user_story(description):
    values = {"from_comment": "False",
              "project": "6",
              "subject": description}

    us_url = SERVER_NAME + "api/v1/userstories"
    us_response = post(us_url, headers=head, data=values)
    return us_response.json()["id"]


def user_story_from_attachment(filename):
    userstory_id = create_user_story(path.splitext(path.basename(filename))[0])
    upload(filename, userstory_id)


# print(create_user_story("US from API"))
user_story_from_attachment("downloads/testupload.png")
